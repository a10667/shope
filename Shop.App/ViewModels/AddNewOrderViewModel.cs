﻿using Shop.App.Models;
using Shop.App.Models.Commands;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.Views.Clients;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Shop.App.ViewModels
{
    public class AddNewOrderViewModel : ViewModelBase, IDataErrorInfo
    {
        private RelayCommand _addCommand;

        private string _clientText = "Client:";
        private string _statusText = "Status:";
        private string _numberText = "Number:";
        private string _priceText = "Price:";

        private ClientModel _selectedClient;
        private ObservableCollection<ClientModel> _clients = new ObservableCollection<ClientModel>();
        
        private string _price = "488";
        private string _number = Guid.NewGuid().ToString();
        private BitmapImage? _phote = new BitmapImage(new Uri(@"/Assets/not_found_photo.jpg", UriKind.RelativeOrAbsolute));
        
        private RelayCommand _uploadImageCommand;
        private RelayCommand _openAddNewClientViewCommand;


        private readonly IClientsRepository _clientsRepository;
        private readonly IOrdersRepository _orderRepository;
        private readonly IMessageBoxService _messageBoxService;
        private readonly IFileService<BitmapImage> _fileService;
        private readonly IDialogService _dialogService;

        public AddNewOrderViewModel(
            IOrdersRepository orderRepository, 
            IClientsRepository clientsRepository, 
            IMessageBoxService messageBoxService, 
            IFileService<BitmapImage> fileService, 
            IDialogService dialogService)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
            _clientsRepository = clientsRepository ?? throw new ArgumentNullException(nameof(clientsRepository));
            _fileService = fileService ?? throw new ArgumentNullException(nameof(fileService));
            _dialogService = dialogService ?? throw new ArgumentNullException(nameof(dialogService));

            UpdateClients();
        }

        public RelayCommand AddNewOrderCommand => _addCommand ??= new RelayCommand(async o => {
            string error = this["Price"];
            if (error.Length > 1)
                throw new InvalidOperationException(error);

            var order = new OrderModel(
                OrderNumber, decimal.Parse(Price), false, Photo, SelectedClient
                );
            await _orderRepository.AddNewOrder(order);
            OrderNumber = Guid.NewGuid().ToString();
        });

        public RelayCommand BackCommand => CommandsManager.BackCommand;

        public RelayCommand UploadImageCommand => _uploadImageCommand ??= new RelayCommand(o => {
            if (_dialogService.OpenFileDialog()) 
            {
                var photo = _fileService.Open(_dialogService.FilePath) ?? throw new InvalidOperationException("Не удалось загрузить изображение!");
                Photo = photo;
            }
        });

        public RelayCommand OpenAddNewClientViewCommand => _openAddNewClientViewCommand ??= new RelayCommand(async o => {
            new AddNewClientWindow().OpenAsDialog();
            await UpdateClients();
        })
;
        public ClientModel SelectedClient { get => _selectedClient; set => SetValue(ref _selectedClient, value); }

        public ObservableCollection<ClientModel> Clients { get => _clients; set => SetValue(ref _clients, value); }

        public string NumberText => _numberText;
        public string ClientText => _clientText;
        public string StatusText => _statusText;
        public string PriceText => _priceText;

        public string Price { get => _price; set => SetValue(ref _price, value); }
        public string OrderNumber { get => _number; set => SetValue(ref _number, value); }
        public string Status => "Не выполнен";


        private async Task UpdateClients()
        {
            Clients = new ObservableCollection<ClientModel>(await _clientsRepository.GetClients());
            if (Clients.Count > 0)
                _selectedClient = Clients.First();
        }

        public string Error => throw new NotImplementedException();

        public BitmapImage? Photo { get => _phote;  set => SetValue (ref _phote, value); }

        public string this[string columnName] 
        {
            get 
            {
                switch (columnName) 
                {
                    case "Price":
                        if (!decimal.TryParse(Price.Replace('.', ','), out var price)) return "Цена введена некорректно!!!";
                        if (price is 0) return "Цена не может быть 0";
                        break;
                }
                return "";
            }
        }
    }
}
