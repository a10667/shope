﻿using Shop.App.Models;
using Shop.App.Models.Commands;
using Shop.App.Models.Entity;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.Views.Orders;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.App.ViewModels
{
    public class OrdersViewModels : ViewModelBase
    {
        private readonly IOrdersRepository _ordersRepository;
        private readonly IMessageBoxService _messageBoxService;

        private OrderModel _selectedOrder = OrderModel.Empty;

        private RelayCommand _openAddnewOrderViewCommand;
        private RelayCommand _deleteOrderCommand;
        private RelayCommand _updateOrderCommand;

        private ObservableCollection<OrderModel> _orders = new ObservableCollection<OrderModel>();


        public OrdersViewModels(IOrdersRepository ordersRepository, IMessageBoxService messageBoxService)
        {
            _ordersRepository = ordersRepository ?? throw new ArgumentNullException(nameof(ordersRepository));
            UpdateOrders();
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
        }

        private async Task UpdateOrders()
        {
            Orders = new ObservableCollection<OrderModel>(await GetAllOrders());
            _selectedOrder = OrderModel.Empty;
        }

        public OrderModel SelectedOrder 
        { 
            get => _selectedOrder;  
            set => SetValue(ref _selectedOrder, value); 
        } 

        public ObservableCollection <OrderModel> Orders { get => _orders; set => SetValue(ref _orders, value); }
        public RelayCommand OpenAddNewOrderViewCommand => _openAddnewOrderViewCommand ??= new RelayCommand(async o => 
        {
            new AddNewOrderWindow().OpenAsDialog();
            await UpdateOrders();

        });
        public RelayCommand BackCommand => CommandsManager.BackCommand;
        public RelayCommand DeleteOrderCommand => _deleteOrderCommand ??= new RelayCommand(async o =>
        {
            await _ordersRepository.DeleteOrder(SelectedOrder);
            _messageBoxService.Show("Заказ удален");
            await UpdateOrders();
        });

        public RelayCommand UpdateOrderCommand => _updateOrderCommand ??= new RelayCommand(async o => 
        {
            new UpdateOrderWindow(SelectedOrder).ShowDialog();
            await UpdateOrders();
        });

        private async Task<IEnumerable<OrderModel>> GetAllOrders() => await _ordersRepository.GetOrders();
    }
}
