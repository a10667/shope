﻿using Microsoft.EntityFrameworkCore;
using Shop.App.Models;
using Shop.App.Models.Commands;
using Shop.App.Models.Entity;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.Views.Clients;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.App.ViewModels
{
    public class ClientsViewModels : ViewModelBase
    {
        private readonly IClientsRepository _clientsRepository;
        private readonly IMessageBoxService _messageBoxService;
        
        private ClientModel _selectedClient;

        private RelayCommand _openAddNewClientViewCommand;
        private RelayCommand _deleteClientCommand;
        
        private ObservableCollection<ClientModel> _clients = new ObservableCollection<ClientModel>();

        public ClientsViewModels(IClientsRepository clientsRepository, IMessageBoxService messageBoxService)
        {
            _clientsRepository = clientsRepository ?? throw new ArgumentNullException(nameof(clientsRepository));
            _messageBoxService = messageBoxService;
            UpdateClients();

        }

        public ClientModel SelectedClient
        {
            get => _selectedClient;
            set => SetValue(ref _selectedClient, value, nameof(SelectedClient));
        }

        public ObservableCollection<ClientModel> Clients { get => _clients; set => SetValue (ref _clients, value); }


        public RelayCommand OpenAddNewClientView => _openAddNewClientViewCommand ??= new RelayCommand(async o =>
        {

            new AddNewClientWindow().OpenAsDialog();
            await UpdateClients();
        });

        private async Task UpdateClients()
        {
            Clients = new ObservableCollection<ClientModel>(await _clientsRepository.GetClients());
            _selectedClient = ClientModel.Empty;

        }

        public RelayCommand UpdateClientCommand => _deleteClientCommand ??= new RelayCommand(async o => 
        {
            new UpdateClientWindow(SelectedClient).ShowDialog();
            await UpdateClients();
        });

        public RelayCommand BackCommand => CommandsManager.BackCommand;

    }
}
