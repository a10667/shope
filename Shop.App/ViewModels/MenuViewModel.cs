﻿using Shop.App.Models.Commands;
using Shop.App.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Shop.App.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private string _orderButtonText = "Orders";
        private string _clientButtonText = "Clients";
        private string _helpButtonText = "Help";
        private string _exitButtonText = "Exit";

        private RelayCommand? _openOrdresCommand;
        private RelayCommand? _openClientsCommand;
        private RelayCommand? _openHelpCommand;
        private RelayCommand? _exitCommand;

       
        
        public string OrderButtonText { get => _orderButtonText; set => SetValue(ref _orderButtonText, value, nameof(OrderButtonText)); }
        public string ClientsButtonText { get => _clientButtonText; set => SetValue(ref _clientButtonText, value, nameof(ClientsButtonText)); }
        public string HelpButtonText { get => _helpButtonText; set => SetValue(ref _helpButtonText, value, nameof(HelpButtonText)); }
        public string ExitButtonText { get => _exitButtonText; set => SetValue(ref _exitButtonText, value, nameof(ExitButtonText)); }
        
        public RelayCommand OpenOrdresCommand => _openOrdresCommand ??= new (o => {
            var menuWin = o as MainWindow;
            var window = new OrdersWindow();
            window.OpenAsDialog();
        });
        public RelayCommand OpenClientsCommand => _openClientsCommand ??= new(o => {
            var menuWin = o as MainWindow;
            var clientsWin = new ClientsWindow();
            clientsWin.OpenAsDialog();
        });
        public RelayCommand OpenHelpCommand => _openHelpCommand ??= new(o => {
            new HelpWindow().ShowDialog();
        });
        public RelayCommand ExitCommand => CommandsManager.BackCommand;
    }
}
