﻿using Shop.App.Models;
using Shop.App.Models.Commands;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Shop.App.ViewModels
{
    internal class UpdateOrderViewModel : ViewModelBase, IDataErrorInfo
    {
        private readonly OrderModel _selectedOrder;
        
        private readonly IOrdersRepository _orderRepository;
        private readonly IMessageBoxService _messageBoxService;
        private readonly IFileService<BitmapImage> _fileService;
        private readonly IDialogService _dialogService;

        private RelayCommand _uploadImageCommand;
        private RelayCommand _updateOrderCommand;

        private BitmapImage _photo;
        private string _price;
        private StatusView _status;
        private string _fisrtname;
        private string _lastname;
        private string _secondname;
        private string _number;
        private ObservableCollection<StatusView> _statuses = new ObservableCollection<StatusView> {
            new StatusView(true),
            new StatusView(false),
        };

        public UpdateOrderViewModel(
            OrderModel selectedOrder, 
            IOrdersRepository orderRepository, 
            IMessageBoxService messageBoxService, 
            IFileService<BitmapImage> fileService, 
            IDialogService dialogService)
        {
            this._selectedOrder = selectedOrder ?? throw new NullReferenceException(nameof(selectedOrder));
            this._orderRepository = orderRepository ?? throw new NullReferenceException(nameof(orderRepository));
            _fileService = fileService ?? throw new ArgumentNullException(nameof(fileService));
            _dialogService = dialogService ?? throw new ArgumentNullException(nameof(dialogService));
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            _status = _statuses.First(s => s.Value == _status.Value);
            _photo = selectedOrder.Photo;
            _price = selectedOrder.Price.ToString();

             _fisrtname = selectedOrder.Client.FirstName;
            _lastname = selectedOrder.Client.LastName;
            _secondname = selectedOrder.Client.SecondName;
            _number = selectedOrder.Number;
        }


        public RelayCommand BackCommand => CommandsManager.BackCommand;

        public RelayCommand UploadImageCommand => _uploadImageCommand ??= new RelayCommand(o => {
            if (_dialogService.OpenFileDialog())
            {
                var photo = _fileService.Open(_dialogService.FilePath) ?? throw new InvalidOperationException("Не удалось загрузить изображение!");
                Photo = photo;
            }
        });

        public RelayCommand UpdateOrderCommand => _updateOrderCommand ??= new RelayCommand(async o => {
            string error = this["Price"];
            if (error.Length > 1)
                throw new InvalidOperationException(error);

            var order = new OrderModel(
                _selectedOrder.Number, decimal.Parse(Price), Status.Value, Photo, _selectedOrder.Client);
            await _orderRepository.UpdateOrder(order);
            _messageBoxService.Show("Заказ обновлен");
        });


        public BitmapImage Photo { get => _photo; set => SetValue(ref _photo, value); }
        public string Price { get => _price; set => SetValue(ref _price, value); }

        public StatusView Status { get => _status; set => SetValue(ref _status, value); }
        public string Firstname { get => _fisrtname; set => SetValue(ref _fisrtname, value, nameof(Firstname)); }
        public string Lastname { get => _lastname; set => SetValue(ref _lastname, value, nameof(Lastname)); }
        public string Secondname { get => _secondname; set => SetValue(ref _secondname, value, nameof(Secondname)); }
        public string OrderNumber { get => _number; set => SetValue(ref _number, value); }

        public string Error => throw new NotImplementedException();
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Price":
                        Price = Price.Replace('.', ',');
                        if (!decimal.TryParse(Price, out var price)) return "Цена введена некорректно!!!";
                        if (price is 0) return "Цена не может быть 0";
                        break;
                }
                return "";
            }
        }

        public ObservableCollection<StatusView> Statuses { get => _statuses; set => SetValue (ref _statuses, value); }
        public struct StatusView
        {
            public StatusView(bool status) : this()
            {
                this.Value = status;
            }

            public bool Value { get; set; }

            public override string ToString() => Value ? "Выполнен" : "Не выполнен";
        }
    }
}
