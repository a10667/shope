using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Shop.App.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public void SetValue<T>(ref T dest, T value, [CallerMemberName]string prop = null) 
        {
            if (dest == null) 
                throw new ArgumentNullException(nameof(dest));

            if (dest.Equals(value))
                return;

            dest = value;
            OnPropertyChanged(prop);
        }

        protected virtual void OnPropertyChanged(string prop = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
