﻿using Shop.App.Models;
using Shop.App.Models.Commands;
using Shop.App.Models.Entity;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.App.ViewModels
{
    internal class UpdateClientViewModel : ViewModelBase, IDataErrorInfo
    {
        private string _fisrtnameText = "Fisrtname:";
        private string _lastnameText = "Lastname:";
        private string _secondnameText = "Secondname:";
        private string _cityText = "City:";
        private string _streetText = "Street:";
        private string _numberText = "Number:";
        private string _addressTitleText = "Address:";
        private string _phoneText = "Phone:";

        private List<string> _props = new List<string>(){"Phone", "Fisrtname", "Secondname", "Lastname", "Number" };


        private readonly IClientsRepository _clientRepository;
        private readonly IMessageBoxService _messageBoxService;

        private RelayCommand _updateClientCommand;
        
        private ObservableCollection<CityModel> _cities;
        private ObservableCollection<StreetModel> _streets;

        private CityModel _selectedCity;
        private StreetModel _selectedStreet;
        private string _fisrtname = "";
        private string _lastname = "";
        private string _secondname = "";
        private string _phone = "";
        private string _city = "";
        private string _street = "";
        private string _number = "";
        private ClientModel _clientModel;

        public UpdateClientViewModel(ClientModel clientModel, IClientsRepository clientRepository, IMessageBoxService messageBoxService)
        {
            _clientRepository = clientRepository;
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));


            using var context = new ShopBDContext();
            _cities = new ObservableCollection<CityModel>(context.Cities.Select(c => new CityModel(c.Id, c.Value)));
            _streets = new ObservableCollection<StreetModel>(context.Streets.Select(s => new StreetModel(s.Id, s.Value)));

            _fisrtname = clientModel.FirstName;
            _lastname = clientModel.LastName;
            _secondname = clientModel.SecondName;
            _phone = clientModel.Phone;
            _city = clientModel.Address.City;
            _street = clientModel.Address.Street;
            _number = clientModel.Address.Number;

            _clientModel = clientModel;

            if (_cities.Any())
            {
                _selectedCity = _cities.First(c => c.Value == clientModel.Address.City);
            }
            if (_streets.Any())
            {
                _selectedStreet = _streets.First(c => c.Value == clientModel.Address.Street);
            }

        }


        public RelayCommand UpdateClientCommand => _updateClientCommand ??= new RelayCommand(async o => {
            if (_props.Select(c => this[c]).Any(s => s.Length > 0)) 
            {
                throw new InvalidOperationException("Не все поля заполнены корректно !!!");
            }
            var client = 
            new ClientModel(
                _clientModel.Id, Firstname, Secondname, Lastname, Phone, 
                    new AddressModel(_clientModel.Address.Id, SelectedStreet.Value, Number, SelectedCity.Value));
            await _clientRepository.UpdateClient(client);
            _messageBoxService.Show("Клиент обнавлен");
        });

        public RelayCommand BackCommand => CommandsManager.BackCommand;

        public ObservableCollection<CityModel> Cities => _cities;
        public ObservableCollection<StreetModel> Streets => _streets;

        public CityModel SelectedCity 
        {
            get => _selectedCity;
            set => SetValue(ref _selectedCity, value, nameof(SelectedCity));
        }

        public StreetModel SelectedStreet
        {
            get => _selectedStreet;
            set => SetValue(ref _selectedStreet, value, nameof(SelectedStreet));
        }

        public string FirstnameText => _fisrtnameText;
        public string LastnameText => _lastnameText;
        public string SecondnameText => _secondnameText;
        public string PhoneText => _phoneText;
        public string CityText => _cityText;
        public string StreetText => _streetText;
        public string NumberText => _numberText;
        public string AddressTitleText => _addressTitleText;


        public string Firstname     { get => _fisrtname; set => SetValue(ref _fisrtname, value, nameof(Firstname)); }
        public string Lastname      { get => _lastname; set => SetValue(ref _lastname, value, nameof(Lastname)); }
        public string Secondname    { get => _secondname; set => SetValue(ref _secondname, value, nameof(Secondname)); }
        public string Phone         { get => _phone; set => SetValue(ref _phone, value, nameof(Phone)); }
        public string City          { get =>  _city; set => SetValue(ref _city, value, nameof(City)); }
        public string Street        { get =>  _street; set => SetValue(ref _street, value, nameof(Street)); }
        public string Number        { get =>  _number; set => SetValue(ref _number, value, nameof(Number)); }

        public string Error => throw new NotImplementedException();

        public string this[string columnName] 
        {
            get 
            {
                string error = "";
                string emptyFiledError = "Поле не должно быть пустым!!!";
                switch (columnName) 
                {
                    case "Firstname":
                        if (Firstname.Length <= 0 || string.IsNullOrEmpty(Firstname) || string.IsNullOrWhiteSpace(Firstname)) return emptyFiledError;
                        break;
                    case "Lastname":
                        if (Lastname.Length <= 0 || string.IsNullOrEmpty(Lastname) || string.IsNullOrWhiteSpace(Lastname)) return emptyFiledError;
                        break;
                    case "Secondname":
                        if (Secondname.Length <= 0 || string.IsNullOrEmpty(Secondname) || string.IsNullOrWhiteSpace(Secondname)) return emptyFiledError;
                        break;
                    case "City":
                        if (City.Length <= 0 || string.IsNullOrEmpty(City) || string.IsNullOrWhiteSpace(City)) return emptyFiledError;
                        break;
                    case "Street":
                        if (Street.Length <= 0 || string.IsNullOrEmpty(Street) || string.IsNullOrWhiteSpace(Street)) return emptyFiledError;
                        break;

                    case "Number":
                        if (Number.Length <= 0 || string.IsNullOrEmpty(Number) || string.IsNullOrWhiteSpace(Number)) return emptyFiledError;
                        break;

                    case "Phone":
                        if (Phone.Length != 11 || Phone.Length <= 0)
                            return "Номер должен состоять из 11 символов !!!";
                        if (Phone.Any(c => !char.IsDigit(c))) return "Номер должен состиять из цифр";
                        break;
                }
                return error;
            }
        }
    }
}
