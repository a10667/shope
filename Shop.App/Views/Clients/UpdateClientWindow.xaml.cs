﻿using Shop.App.Models;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop.App.Views.Clients
{
    /// <summary>
    /// Логика взаимодействия для UpdateClientWindow.xaml
    /// </summary>
    public partial class UpdateClientWindow : Window
    {
        private ClientModel selectedClient;

        public UpdateClientWindow(ClientModel selectedClient)
        {
            InitializeComponent();
            this.selectedClient = selectedClient;
            var service = new WPFMessageBoxService();
            DataContext = new UpdateClientViewModel(selectedClient, new EfClientRepository(service), service);
        }
    }
}
