﻿using Shop.App.Models;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop.App.Views.Clients
{
    /// <summary>
    /// Логика взаимодействия для AddNewClientWindow.xaml
    /// </summary>
    public partial class AddNewClientWindow : Window, IView
    {
        public AddNewClientWindow()
        {
            InitializeComponent();
            var service = new WPFMessageBoxService();
            DataContext = new AddNewClientViewModel(new EfClientRepository(service),  service);
        }
        public void Open()
        {
            Show();
        }

        public void OpenAsDialog()
        {
            ShowDialog();
        }

        public void Shutdown()
        {
            Close();
        }
    }
}
