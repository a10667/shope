﻿using Shop.App.Models;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.ViewModels;
using Shop.App.Views.Clients;
using System.Windows;

namespace Shop.App.Views
{
    /// <summary>
    /// Логика взаимодействия для Clients.xaml
    /// </summary>
    public partial class ClientsWindow : Window, IView
    {
        public ClientsWindow()
        {
            InitializeComponent();
            var service = new WPFMessageBoxService();
            DataContext = new ClientsViewModels(new EfClientRepository(service), service);
        }

        public void Open()
        {
            Show();
        }
        public void OpenAsDialog()
        {
            ShowDialog();
        }

        public void Shutdown()
        {
            Close();
        }


        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as ClientsViewModels;
            vm.UpdateClientCommand.Execute(vm.SelectedClient);
        }
    }
}
