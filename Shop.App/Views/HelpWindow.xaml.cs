﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop.App.Views
{
    /// <summary>
    /// Логика взаимодействия для HelpWindow.xaml
    /// </summary>
    public partial class HelpWindow : Window
    {
        public HelpWindow()
        {
            InitializeComponent();
        }

        private void Doc_Loaded(object sender, RoutedEventArgs e)
        {
            using var fs = new FileStream(@"help.rtf", FileMode.Open);
            TextRange textRange = new TextRange(Doc.ContentStart, Doc.ContentEnd);
            textRange.Load(fs, DataFormats.Rtf);
        }
    }
}
