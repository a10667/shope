﻿using Shop.App.Models;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.ViewModels;
using Shop.App.Views.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop.App.Views
{
    /// <summary>
    /// Логика взаимодействия для OrdersWindow.xaml
    /// </summary>
    public partial class OrdersWindow : Window, IView
    {
        public OrdersWindow()
        {
            InitializeComponent();
            var service = new WPFMessageBoxService();
            DataContext = new OrdersViewModels(new EfOrdersRepository(service), service);
        }

        public void Open()
        {
            Show();
        }

        public void OpenAsDialog()
        {
            ShowDialog();
        }

        public void Shutdown()
        {
            Close();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as OrdersViewModels;
            vm.DeleteOrderCommand.Execute(vm.SelectedOrder);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as OrdersViewModels;
            vm.UpdateOrderCommand.Execute(vm.SelectedOrder);
        }
    }
}
