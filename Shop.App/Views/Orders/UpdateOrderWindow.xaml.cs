﻿using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop.App.Views.Orders
{
    /// <summary>
    /// Логика взаимодействия для UpdateOrderWindow.xaml
    /// </summary>
    public partial class UpdateOrderWindow : Window
    {
        public UpdateOrderWindow(Models.OrderModel selectedOrder)
        {
            InitializeComponent();
            var service = new WPFMessageBoxService();

            DataContext = new UpdateOrderViewModel(
                selectedOrder, 
                new EfOrdersRepository(service), service, new ImageFileService(), new WpfDialogService());
        }
    }
}
