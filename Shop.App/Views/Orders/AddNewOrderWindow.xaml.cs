﻿using Shop.App.Models;
using Shop.App.Models.Repositories;
using Shop.App.Models.Services;
using Shop.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Shop.App.Views.Orders
{
    /// <summary>
    /// Логика взаимодействия для AddNewOrderWindow.xaml
    /// </summary>
    public partial class AddNewOrderWindow : Window, IView
    {
        public AddNewOrderWindow()
        {
            InitializeComponent();
            var service = new WPFMessageBoxService();
            DataContext = new AddNewOrderViewModel(
                new EfOrdersRepository(service), 
                new EfClientRepository(service), 
                service ,
                new ImageFileService(), 
                new WpfDialogService());
        }

        public void Open()
        {
            Show();
        }

        public void OpenAsDialog()
        {
            ShowDialog();
        }

        public void Shutdown()
        {
            Close();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
