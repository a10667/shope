﻿namespace Shop.App.Models
{
    public record AddressModel (int Id, string Street, string Number, string City);
}