﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Shop.App.Models.Commands
{
    internal class BackCommand : RelayCommand
    {
        public BackCommand(Func<object, bool> canExecute = null) : base(CloseWindow, canExecute)
        {
        }

        private static void CloseWindow(object win) 
        {
            if (win is Window w) 
            {
                w.Close();
                return;
            } 
        }
    }
}
