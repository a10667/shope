﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.App.Models
{
    public interface IView
    {
        void Open();
        void OpenAsDialog();
        void Shutdown();
    }
}
