﻿using Microsoft.EntityFrameworkCore;
using Shop.App.Models.Entity;
using Shop.App.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.App.Models.Repositories
{
    internal class EfClientRepository : IClientsRepository
    {
        private readonly IMessageBoxService _messageBoxService;

        public EfClientRepository(IMessageBoxService messageBoxService)
        {
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
        }

        public async Task AddNewClient(ClientModel clientModel)
        {
            using var context = new ShopBDContext();
            var streetid = (await context.Streets.FirstAsync(s => s.Value == clientModel.Address.Street)).Id;
            var cityId = (await context.Cities.FirstAsync(s => s.Value == clientModel.Address.City)).Id;
            var addressId = 0;
            if (IsNewAddressNeeded(clientModel, context, streetid, cityId))
            {
                await context.Addresses.AddAsync(new Address
                {
                    StreetId = streetid,
                    CityId = cityId,
                    Number = clientModel.Address.Number
                });

                await context.SaveChangesAsync();
                addressId = context.Addresses.OrderBy(a=>a.Id).Last().Id;
            } 
            else 
            {
                addressId = (await context.Addresses.FirstAsync(a => a.Number == clientModel.Address.Number && a.CityId == cityId && a.StreetId == streetid)).Id;
            }


            await context.Clients.AddAsync(new Client
            {
                Firstname = clientModel.FirstName,
                Secondname = clientModel.SecondName,
                Lastname = clientModel.LastName,
                AddressId = addressId, Phone = clientModel.Phone
            });
            await context.SaveChangesAsync();
            _messageBoxService.Show("Клиент добавлен");
        }

        private static bool IsNewAddressNeeded(ClientModel clientModel, ShopBDContext context, int streetid, int cityId)
        {
            return context.Addresses.FirstOrDefault(a => a.Number == clientModel.Address.Number && a.CityId == cityId && a.StreetId == streetid) == null;
        }

        public async Task<IEnumerable<ClientModel>> GetClients()
        {
            using var context = new ShopBDContext();
            var clients = context.Clients.Select(c => new ClientModel(c.Id, c.Firstname, c.Secondname, c.Lastname, c.Phone,
                                new((int)c.AddressId, c.Address.Street.Value, c.Address.Number, c.Address.City.Value)));
            return clients.ToList();
        }

        public async Task UpdateClient(ClientModel clientModel)
        {
            using var context = new ShopBDContext();
            
            
            var address = await context.Addresses.FirstAsync(c => c.Id == clientModel.Address.Id);

            address.StreetId = (await context.Streets.FirstAsync(s => s.Value == clientModel.Address.Street)).Id;
            address.CityId = (await context.Cities.FirstAsync(c => c.Value == clientModel.Address.City)).Id;
            address.Number = clientModel.Address.Number;

            await context.SaveChangesAsync();

            var client = await context.Clients.FirstAsync(c => c.Id == clientModel.Id);
            client.Firstname = clientModel.FirstName;
            client.Secondname = clientModel.SecondName;
            client.Lastname = clientModel.LastName;
            client.Phone = clientModel.Phone;
            await context.SaveChangesAsync();
        }
    }
}
