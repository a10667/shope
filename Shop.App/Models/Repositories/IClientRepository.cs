﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.App.Models.Repositories
{
    public interface IClientsRepository
    {
        Task<IEnumerable<ClientModel>> GetClients();
        Task AddNewClient(ClientModel? clientModel);
        Task UpdateClient(ClientModel client);
    }
}
