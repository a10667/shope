﻿using Microsoft.EntityFrameworkCore;
using Shop.App.Models.Entity;
using Shop.App.Models.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Shop.App.Models.Repositories
{
    internal class EfOrdersRepository : IOrdersRepository
    {
        private readonly IMessageBoxService _messageBoxService;

        public EfOrdersRepository(IMessageBoxService messageBoxService)
        {
            _messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService)); 
        }

        public async Task AddNewOrder(OrderModel orderModel)
        {
            using var context = new ShopBDContext();
            context.Orders.Add(new Order 
            {
                Status = false,
                Price = orderModel.Price,
                Number = orderModel.Number,
                Photo = ImageToBytes(orderModel.Photo)
            });
            await context.SaveChangesAsync();

            var clientId = (await context.Clients.FirstAsync(c => c.Id == orderModel.Client.Id)).Id;

            await context.Database.ExecuteSqlRawAsync($"insert into [ClientsOrders] ([ClientId], [OrderNumber]) values ({clientId}, '{orderModel.Number}')");
            await context.SaveChangesAsync();
            _messageBoxService.Show("Заказ создан");
        }

        public async Task DeleteOrder(OrderModel selectedOrder)
        {
            using var context = new ShopBDContext();
            var order = await context.Orders.FirstAsync(o => o.Number == selectedOrder.Number);
            context.Orders.Remove(order);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<OrderModel>> GetOrders()
        {
            using var context = new ShopBDContext();
            var orders = context.Orders.Select(o => new OrderModel(o.Number, o.Price, o.Status, BytesToImage(o.Photo), new ClientModel(
                o.Clients.First().Id, o.Clients.First().Firstname, o.Clients.First().Secondname, o.Clients.First().Lastname, o.Clients.First().Phone,
                                new((int)o.Clients.First().AddressId, o.Clients.First().Address.Street.Value, o.Clients.First().Address.Number, o.Clients.First().Address.City.Value))));
            return orders.ToList();
        }

        public async Task UpdateOrder(OrderModel selectedOrder)
        {
            using var context = new ShopBDContext();
            var order = await context.Orders.FirstAsync(s => s.Number == selectedOrder.Number);

            order.Price = selectedOrder.Price;
            order.Status = selectedOrder.Status;
            order.Photo = ImageToBytes(selectedOrder.Photo);

            await context.SaveChangesAsync();
        }

        private static BitmapImage? BytesToImage(byte[]? bytes) 
        {
            if (bytes == null) return null;

            var image = new BitmapImage();

            using var ms = new MemoryStream(bytes, 0, bytes.Length);

            image.BeginInit();
            image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.StreamSource = ms;
            image.EndInit();

            image.Freeze();
            return image;
        }

        private static byte[] ImageToBytes(BitmapImage image)
        {
            byte[] data;

            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));
            using var ms = new MemoryStream();
            encoder.Save(ms);
            data = ms.ToArray();
            return data;
        }
    
    }


}
