﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.App.Models.Repositories
{
    public interface IOrdersRepository
    {
        Task<IEnumerable<OrderModel>> GetOrders();
        Task AddNewOrder(OrderModel? orderModel);
        Task DeleteOrder(OrderModel selectedOrder);
        Task UpdateOrder(OrderModel selectedOrder);
    }
}