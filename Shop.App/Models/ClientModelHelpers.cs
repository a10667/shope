﻿using Shop.App.Models.Entity;

namespace Shop.App.Models
{
    internal static class ClientModelHelpers
    {
        public static ClientModel From(Client c)
        {
            return new ClientModel(c.Id, c.Firstname, c.Secondname, c.Lastname, c.Phone,
                                new((int)c.AddressId, c.Address.Street.Value, c.Address.Number, c.Address.City.Value));
        }
    }
}