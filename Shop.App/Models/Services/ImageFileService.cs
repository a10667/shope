﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Shop.App.Models.Services
{
    public class ImageFileService : IFileService<BitmapImage>
    {
        public BitmapImage Open(string filename)
        {
            using var ms = new MemoryStream(File.ReadAllBytes(filename));
            BitmapImage image = new();
            ms.Position = 0;
            image.BeginInit();
            image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.StreamSource = ms;
            image.EndInit();

            return image;
        }

        public void Save(string filename, BitmapImage fileInstance)
        {
            throw new NotImplementedException();
        }
    }
}
