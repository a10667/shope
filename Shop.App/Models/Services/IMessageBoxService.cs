﻿namespace Shop.App.Models.Services
{
    public interface IMessageBoxService
    {
        void Show(string message);
        void ShowError(string message);
    }
}