﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Shop.App.Models.Services
{
    internal class WPFMessageBoxService : IMessageBoxService
    {
        public void Show(string message) 
        { 
            MessageBox.Show( message, "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
        public void ShowError(string message) 
        {
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
