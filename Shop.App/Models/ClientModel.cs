﻿namespace Shop.App.Models
{
    public record ClientModel(int Id, string FirstName, string SecondName, string LastName, string Phone, AddressModel Address)
    {
        public static ClientModel Empty => new ClientModel(default, string.Empty, string.Empty, string.Empty, string.Empty, null);
        public override string ToString()
        {
            return $"{FirstName} {SecondName} {LastName}";
        }
    }
}
