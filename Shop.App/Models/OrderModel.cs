﻿using System.Windows.Media.Imaging;

namespace Shop.App.Models
{
    public record OrderModel(string Number, decimal Price, bool Status, BitmapImage? Photo, ClientModel Client)
    {
        public static OrderModel Empty => new OrderModel(string.Empty, default, false, null, null);
    }
}
