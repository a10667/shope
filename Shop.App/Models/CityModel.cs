﻿namespace Shop.App.Models
{
    public record CityModel(int Id, string Value)
    {
        public override string ToString()
        {
            return Value;
        }
    }
}