﻿using System;
using System.Collections.Generic;

namespace Shop.App.Models.Entity
{
    public partial class Client
    {
        public Client()
        {
            OrderNumbers = new HashSet<Order>();
        }

        public int Id { get; set; }
        public int? AddressId { get; set; }
        public string Firstname { get; set; } = null!;
        public string Secondname { get; set; } = null!;
        public string Lastname { get; set; } = null!;
        public string Phone { get; set; } = null!;

        public virtual Address? Address { get; set; }

        public virtual ICollection<Order> OrderNumbers { get; set; }
    }
}
