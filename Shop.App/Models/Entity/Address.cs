﻿using System;
using System.Collections.Generic;

namespace Shop.App.Models.Entity
{
    public partial class Address
    {
        public Address()
        {
            Clients = new HashSet<Client>();
        }

        public int Id { get; set; }
        public int? StreetId { get; set; }
        public int? CityId { get; set; }
        public string Number { get; set; } = null!;

        public virtual City? City { get; set; }
        public virtual Street? Street { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
    }
}
