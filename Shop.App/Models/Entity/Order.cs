﻿using System;
using System.Collections.Generic;

namespace Shop.App.Models.Entity
{
    public partial class Order
    {
        public Order()
        {
            Clients = new HashSet<Client>();
        }

        public string Number { get; set; } = null!;
        public bool Status { get; set; }
        public decimal Price { get; set; }
        public byte[]? Photo { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
    }
}
