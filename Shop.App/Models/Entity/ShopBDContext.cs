﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Shop.App.Models.Entity
{
    public partial class ShopBDContext : DbContext
    {
        private string _connectionString = "";
        public ShopBDContext()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            Database.EnsureCreated();
             //
        }

        public ShopBDContext(DbContextOptions<ShopBDContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; } = null!;
        public virtual DbSet<City> Cities { get; set; } = null!;
        public virtual DbSet<Client> Clients { get; set; } = null!;
        public virtual DbSet<Order> Orders { get; set; } = null!;
        public virtual DbSet<Street> Streets { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
                //optionsBuilder.UseSqlServer("Data Source=PLRX-00011434\\SQLEXPRESS;Initial Catalog=ShopBD;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address");

                entity.Property(e => e.Number).HasMaxLength(50);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Addresses)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK__Address__CityId__3B75D760");

                entity.HasOne(d => d.Street)
                    .WithMany(p => p.Addresses)
                    .HasForeignKey(d => d.StreetId)
                    .HasConstraintName("FK__Address__StreetI__3A81B327");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("City");

                entity.Property(e => e.Value).HasMaxLength(255);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("Client");

                entity.Property(e => e.Firstname).HasMaxLength(50);

                entity.Property(e => e.Lastname).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(11);

                entity.Property(e => e.Secondname).HasMaxLength(50);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Clients)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK__Client__AddressI__3E52440B");

                entity.HasMany(d => d.OrderNumbers)
                    .WithMany(p => p.Clients)
                    .UsingEntity<Dictionary<string, object>>(
                        "ClientsOrder",
                        l => l.HasOne<Order>().WithMany().HasForeignKey("OrderNumber").HasConstraintName("FK__ClientsOr__Order__44FF419A"),
                        r => r.HasOne<Client>().WithMany().HasForeignKey("ClientId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK__ClientsOr__Clien__440B1D61"),
                        j =>
                        {
                            j.HasKey("ClientId", "OrderNumber").HasName("PK__ClientsO__DAD244509FF4DED1");

                            j.ToTable("ClientsOrders");

                            j.IndexerProperty<string>("OrderNumber").HasMaxLength(255);
                        });
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => e.Number)
                    .HasName("PK__Order__78A1A19C8AD36D49");

                entity.ToTable("Order");

                entity.Property(e => e.Number).HasMaxLength(255);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<Street>(entity =>
            {
                entity.ToTable("Street");

                entity.Property(e => e.Value).HasMaxLength(255);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
