﻿using System;
using System.Collections.Generic;

namespace Shop.App.Models.Entity
{
    public partial class Street
    {
        public Street()
        {
            Addresses = new HashSet<Address>();
        }

        public int Id { get; set; }
        public string Value { get; set; } = null!;

        public virtual ICollection<Address> Addresses { get; set; }
    }
}
