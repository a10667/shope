﻿namespace Shop.App.Models
{
    public record StreetModel(int Id, string Value) 
    {
        public override string ToString()
        {
            return Value;
        }
    }
}