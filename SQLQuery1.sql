create database [ShopBD];
go 

use [ShopBD];
go

create table [Street] (
	[Id]			int identity primary key,
	[Value]			nvarchar (255) not null,
); 
go

create table [City] (
	[Id]			int identity primary key,
	[Value]			nvarchar (255) not null,
); 
go

create table [Address] (
	[Id]			int identity primary key,
	[StreetId]		int foreign key references [Street]([Id]),
	[CityId]		int foreign key references [City]([Id]),
	[Number]		nvarchar (50) not null,
);
go

create table [Client] (
	[Id]			int identity primary key,
	[AddressId]		int foreign key references [Address]([Id]),
	[Firstname]		nvarchar (50) not null,
	[Secondname]	nvarchar (50) not null,
	[Lastname]		nvarchar (50) not null,
	[Phone]			nvarchar (11) not null,
);
go

create table [Order] (
	[Number]	nvarchar (255) primary key not null,
	[Status]	bit default (0) not null,
	[Price]		decimal not null,
	[Photo]		varbinary (max),
); 
go

create table [ClientsOrders] (
	[ClientId] int foreign key references [Client]([Id]) on delete cascade,
	[OrderNumber] nvarchar (255) foreign key references [Order]([Number]) on delete cascade,
	primary key ([ClientId], [OrderNumber]),
); 
go

create proc [AddNewOrder] 
	@streetId		int,
	@cityId			int,
	@number			nvarchar (50),
	@firstname		nvarchar (50),
	@secondname		nvarchar (50),
	@lastname		nvarchar (50),
	@phone			nvarchar (11),
	@orderNumber	nvarchar (255),
	@price			decimal
as 
begin
	insert into [Address] ([StreetId], [CityId], [Number]) values (@streetId, @cityId, @number)

	insert into [Client] (AddressId, Firstname, Secondname, Lastname, Phone) values (
		(select max([Id]) from [Address]), @firstname, @secondname, @lastname, @phone)

	insert into [Order] (Number, Price) values (@orderNumber, @price)

	insert into [ClientsOrders] (ClientId, OrderNumber) values ((select max ([Id]) from [Client]), @orderNumber)
end; 
go


INSERT [dbo].[Street] ([Value]) VALUES (N'qwe');
go
INSERT [dbo].[Street] ([Value]) VALUES (N'rty');
go

INSERT [dbo].[Street] ([Value]) VALUES (N'asd');
GO

INSERT [dbo].[City] ([Value]) VALUES (N'123')
GO

INSERT [dbo].[City] ([Value]) VALUES (N'456')
GO

INSERT [dbo].[City] ([Value]) VALUES (N'789')
GO

INSERT [dbo].[Address] ([StreetId], [CityId], [Number]) VALUES (1, 1, N'123')
GO

INSERT [dbo].[Address] ([StreetId], [CityId], [Number]) VALUES (2, 2, N'2')
GO

INSERT [dbo].[Address] ([StreetId], [CityId], [Number]) VALUES (1, 2, N'2')
GO

INSERT [dbo].[Address] ([StreetId], [CityId], [Number]) VALUES (2, 1, N'1')
GO

INSERT [dbo].[Order] ([Number], [Status], [Price], [Photo]) VALUES (N'123-456-789', 0, CAST(456 AS Decimal(18, 0)), NULL)
GO
INSERT [dbo].[Order] ([Number], [Status], [Price], [Photo]) VALUES (N'213-123-123', 0, CAST(123 AS Decimal(18, 0)), NULL)
GO


INSERT [dbo].[Client] ([AddressId], [Firstname], [Secondname], [Lastname], [Phone]) VALUES (1, N'213', N'123', N'asewqe', N'81234567891')
GO

INSERT [dbo].[Client] ([AddressId], [Firstname], [Secondname], [Lastname], [Phone]) VALUES (2, N'qwe', N'qwe', N'rty', N'89876543210')
GO

INSERT [dbo].[ClientsOrders] ([ClientId], [OrderNumber]) VALUES (1, N'213-123-123')
GO

INSERT [dbo].[ClientsOrders] ([ClientId], [OrderNumber]) VALUES (2, N'123-456-789')
GO
